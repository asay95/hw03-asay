#!/usr/bin/bash
##HW3 Task08
## Write a while loop that only exits with a true statement
## Loops until user guesses correctly, otherwise keep making them guess until they're correct.
word="banana"
read -p "Try guessing the secret word: " guess
until [ $guess == $word ]
do
	read -p "Enter another word: " guess	
done 

echo "That's the secret word!"
