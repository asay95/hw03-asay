#!/usr/bin/bash
##HW3 Task09
##Write an if statement that will take positional arguments as inputs
## then return the following output with given words
## If and elif for printing out given words based on the input given by the user
if [ $1 = "data" ]
then
	echo "omelette"
elif [ $1 = "processor" ]
then
	echo "stir fry"
elif [ $1 = "install" ]
then
	echo "baked potato"
elif [ $1 = "banana" ]
then
	echo "burrito"
fi

