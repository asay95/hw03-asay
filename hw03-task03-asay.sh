#!/usr/bin/bash
## HW3 Task03
## Prompt for a name and then greet that name
## Create a name variable to be read from the user
read -p "What is your name? " namevar
echo "Hello there, $namevar!"
