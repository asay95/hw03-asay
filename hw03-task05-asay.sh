#!/usr/bin/bash
##HW3 Task05
## Write an if statement to determine if it's the month being asked
## if [ arg = arg ] true
## elif [ arg = arg2 ] true
## else [ arg != arg ] default

read -p "Is it October? Please only answer with a yes or no. " answer
if [ $answer = "no" ]
then 
	echo "Okay."
elif [ $answer = "yes" ]
then 
	echo "Then it is time to get spoopy! "
else
	echo "Only yes or no for answers please! "
fi
