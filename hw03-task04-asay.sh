#!/usr/bin/bash
## HW3 Task04
## Return the following info using only environment variables
## Number of arguments(#), full list of arguments(*), name of script file(0),
## current user(USER), current working directory(PWD), current $PATH variable(PATH),
## and current shell(SHELL).
echo "$#"
echo "$*"
echo "$0"
echo "$USER"
echo "$PWD"
echo "$PATH"
echo "$SHELL"
