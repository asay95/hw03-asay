#!/usr/bin/bash
## HW03 Task01
## Number of positional parameters with $#
## List of positional parameter with $* to list all arguments in a single string
## Grabbing only the second parameter with $2 to take 2nd argument
echo "Number: $#"
echo "List: $*"
echo "Second: $2"
