#!/usr/bin/bash
## HW3 Task02
## 0 exit code (Success)
## non-zero exit code (Error)
echo "Zero: $?"

## Purposeful error to show a non-zero exit code
ls nonexistantdirectory
echo "Non-Zero: $?"
